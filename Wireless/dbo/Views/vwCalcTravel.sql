﻿CREATE VIEW dbo.vwCalcTravel
AS
SELECT     TOP (100) PERCENT dbo.W6ASSIGNMENTS.W6Key AS AssignmentKey, dbo.W6ENGINEERS.LoginName, dbo.W6ASSIGNMENTS_ENGINEERS.EngineerKey, 
                      dbo.W6ASSIGNMENTS.Task AS TaskKey, dbo.W6ASSIGNMENTS_ENGINEERS.StartTime, dbo.W6ASSIGNMENTS.NonAvailabilityType, dbo.W6TASKS.CallID, 
                      CONVERT(VARCHAR, dbo.W6ASSIGNMENTS.StartTime, 101) AS DOW
FROM         dbo.W6ASSIGNMENTS INNER JOIN
                      dbo.W6ASSIGNMENTS_ENGINEERS ON dbo.W6ASSIGNMENTS.W6Key = dbo.W6ASSIGNMENTS_ENGINEERS.W6Key INNER JOIN
                      dbo.W6ENGINEERS ON dbo.W6ASSIGNMENTS_ENGINEERS.EngineerKey = dbo.W6ENGINEERS.W6Key LEFT OUTER JOIN
                      dbo.W6TASKS ON dbo.W6ASSIGNMENTS.Task = dbo.W6TASKS.W6Key
ORDER BY dbo.W6ASSIGNMENTS_ENGINEERS.EngineerKey, dbo.W6ASSIGNMENTS_ENGINEERS.StartTime
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCalcTravel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCalcTravel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "W6ASSIGNMENTS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 24
         End
         Begin Table = "W6ASSIGNMENTS_ENGINEERS"
            Begin Extent = 
               Top = 119
               Left = 324
               Bottom = 276
               Right = 518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6ENGINEERS"
            Begin Extent = 
               Top = 153
               Left = 598
               Bottom = 261
               Right = 802
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "W6TASKS"
            Begin Extent = 
               Top = 20
               Left = 774
               Bottom = 128
               Right = 993
            End
            DisplayFlags = 280
            TopColumn = 8
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2250
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
    ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCalcTravel';

