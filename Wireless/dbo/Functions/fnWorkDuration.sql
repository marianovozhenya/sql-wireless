﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
Create FUNCTION [dbo].[fnWorkDuration]
(
	-- Add the parameters for the function here
	@WorkDate DATETIME,
	@EngineerKey int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar int
		
	SELECT @ResultVar = [Total Duration]
	FROM [VIVINT].[dbo].[vwScheduledHours]
	WHERE EngineerKey = @EngineerKey and WorkDate = @WorkDate
	
	-- Return the result of the function
	Return @ResultVar
		
END