﻿CREATE TABLE [dbo].[W6OPERATIONAL_ACTIVS_POS_TPS] (
    [W6Key]                   INT NOT NULL,
    [W6SubKey_1]              INT NOT NULL,
    [OperationalPositionType] INT NULL,
    [MinimumCapacity]         INT NULL,
    [MaximumCapacity]         INT NULL,
    CONSTRAINT [W6PK_2080001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK2080001_0_262_] FOREIGN KEY ([OperationalPositionType]) REFERENCES [dbo].[W6OPERATIONAL_POSITION_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK2080001_0_262]
    ON [dbo].[W6OPERATIONAL_ACTIVS_POS_TPS]([OperationalPositionType] ASC) WITH (FILLFACTOR = 50);

