﻿CREATE TABLE [dbo].[W6BFOBJECTS] (
    [ObjectID]        INT           NOT NULL,
    [ObjectType]      INT           NOT NULL,
    [ObjectName]      NVARCHAR (50) NULL,
    [CollectionName]  NVARCHAR (50) NULL,
    [CoreObjectName]  NVARCHAR (50) NULL,
    [ObjectValueType] NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ObjectID] ASC)
);

