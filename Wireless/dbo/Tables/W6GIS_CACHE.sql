﻿CREATE TABLE [dbo].[W6GIS_CACHE] (
    [Origin_Lat]       INT NOT NULL,
    [Origin_Long]      INT NOT NULL,
    [Destination_Lat]  INT NOT NULL,
    [Destination_Long] INT NOT NULL,
    [TravelTime]       INT NULL,
    [Distance]         INT NULL,
    [GISDataSource]    INT NULL,
    [UpdateStatus]     INT NULL
);


GO
CREATE NONCLUSTERED INDEX [W6IX_GIS_TravelTime_New]
    ON [dbo].[W6GIS_CACHE]([TravelTime] ASC);

