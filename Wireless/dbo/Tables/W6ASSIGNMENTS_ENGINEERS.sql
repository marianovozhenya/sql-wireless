﻿CREATE TABLE [dbo].[W6ASSIGNMENTS_ENGINEERS] (
    [W6Key]       INT      NOT NULL,
    [EngineerKey] INT      NOT NULL,
    [Task]        INT      NULL,
    [StartTime]   DATETIME NULL,
    [FinishTime]  DATETIME NULL,
    CONSTRAINT [W6PK_1001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [EngineerKey] ASC),
    CONSTRAINT [W6FK1001_0_8_] FOREIGN KEY ([EngineerKey]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK1001_0_8]
    ON [dbo].[W6ASSIGNMENTS_ENGINEERS]([EngineerKey] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX1001_1]
    ON [dbo].[W6ASSIGNMENTS_ENGINEERS]([Task] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX1001_0_2_3_K_1]
    ON [dbo].[W6ASSIGNMENTS_ENGINEERS]([EngineerKey] ASC, [StartTime] ASC, [FinishTime] ASC, [W6Key] ASC, [Task] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W6_1001_update on W6ASSIGNMENTS_ENGINEERS instead of insert as begin declare @W6Key int, @EngineerKey int, @AE_task int, @AE_starttime datetime, @AE_finishtime datetime select @AE_task=W6ASSIGNMENTS.Task,@AE_starttime=W6ASSIGNMENTS.StartTime, @AE_finishtime=W6ASSIGNMENTS.FinishTime,@W6Key=W6ASSIGNMENTS.W6Key,@EngineerKey=inserted.EngineerKey from W6ASSIGNMENTS,inserted where inserted.W6Key = W6ASSIGNMENTS.W6Key if @AE_task is null begin select @AE_task=-1 end insert into W6ASSIGNMENTS_ENGINEERS (W6Key, EngineerKey,Task, StartTime, FinishTime) values (@W6Key,@EngineerKey, @AE_task, @AE_starttime, @AE_finishtime) end