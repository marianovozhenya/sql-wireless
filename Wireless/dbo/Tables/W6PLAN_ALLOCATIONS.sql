﻿CREATE TABLE [dbo].[W6PLAN_ALLOCATIONS] (
    [W6Key]             INT             NOT NULL,
    [Revision]          INT             NOT NULL,
    [CreatedBy]         NVARCHAR (128)  NOT NULL,
    [TimeCreated]       DATETIME        NOT NULL,
    [CreatingProcess]   INT             NOT NULL,
    [ModifiedBy]        NVARCHAR (128)  NOT NULL,
    [TimeModified]      DATETIME        NOT NULL,
    [ModifyingProcess]  INT             NOT NULL,
    [RelatedPlan]       INT             NULL,
    [PlanScenario]      INT             NULL,
    [AllocatedResource] INT             NULL,
    [PlanDemand]        INT             NULL,
    [Allocation]        INT             NULL,
    [StartTime]         DATETIME        NULL,
    [FinishTime]        DATETIME        NULL,
    [BinaryData]        NVARCHAR (1536) NULL,
    [Context]           INT             NULL,
    CONSTRAINT [W6PK_1] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK1_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK1_4_101_] FOREIGN KEY ([PlanScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key]),
    CONSTRAINT [W6FK1_5_8_] FOREIGN KEY ([AllocatedResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK1_6_4_] FOREIGN KEY ([PlanDemand]) REFERENCES [dbo].[W6PLAN_DEMANDS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX1_3_6_4]
    ON [dbo].[W6PLAN_ALLOCATIONS]([RelatedPlan] ASC, [PlanDemand] ASC, [PlanScenario] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX1_3_4_11]
    ON [dbo].[W6PLAN_ALLOCATIONS]([RelatedPlan] ASC, [PlanScenario] ASC, [Context] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX1_3_4_5_8_9]
    ON [dbo].[W6PLAN_ALLOCATIONS]([RelatedPlan] ASC, [PlanScenario] ASC, [AllocatedResource] ASC, [StartTime] ASC, [FinishTime] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK1_6_4]
    ON [dbo].[W6PLAN_ALLOCATIONS]([PlanDemand] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK1_5_8]
    ON [dbo].[W6PLAN_ALLOCATIONS]([AllocatedResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK1_4_101]
    ON [dbo].[W6PLAN_ALLOCATIONS]([PlanScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK1_3_100]
    ON [dbo].[W6PLAN_ALLOCATIONS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);

