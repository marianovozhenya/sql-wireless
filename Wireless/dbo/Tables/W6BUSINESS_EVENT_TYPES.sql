﻿CREATE TABLE [dbo].[W6BUSINESS_EVENT_TYPES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_63] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX63_2]
    ON [dbo].[W6BUSINESS_EVENT_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);

