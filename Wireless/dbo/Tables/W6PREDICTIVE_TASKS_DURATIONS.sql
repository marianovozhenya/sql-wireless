﻿CREATE TABLE [dbo].[W6PREDICTIVE_TASKS_DURATIONS] (
    [W6Key]              INT            NOT NULL,
    [Revision]           INT            NOT NULL,
    [CreatedBy]          NVARCHAR (128) NOT NULL,
    [TimeCreated]        DATETIME       NOT NULL,
    [CreatingProcess]    INT            NOT NULL,
    [ModifiedBy]         NVARCHAR (128) NOT NULL,
    [TimeModified]       DATETIME       NOT NULL,
    [ModifyingProcess]   INT            NOT NULL,
    [Name]               NVARCHAR (64)  NULL,
    [Region]             INT            NULL,
    [District]           INT            NULL,
    [TaskType]           INT            NULL,
    [CurrentDuration]    INT            NULL,
    [SuggestedDuration]  INT            NULL,
    [NumberOfSuggesters] INT            NULL,
    CONSTRAINT [W6PK_30000031] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000031_4_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK30000031_5_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK30000031_6_56_] FOREIGN KEY ([TaskType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000031_6_56]
    ON [dbo].[W6PREDICTIVE_TASKS_DURATIONS]([TaskType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000031_4_50]
    ON [dbo].[W6PREDICTIVE_TASKS_DURATIONS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000031_5_53]
    ON [dbo].[W6PREDICTIVE_TASKS_DURATIONS]([District] ASC) WITH (FILLFACTOR = 50);


GO
create trigger W630000031_UPDATE_CACHED on W6PREDICTIVE_TASKS_DURATIONS for insert,update as if update(revision) insert into W6OPERATION_LOG select 30000031,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W630000031_DELETE_CACHED on W6PREDICTIVE_TASKS_DURATIONS for delete as insert into W6OPERATION_LOG select 30000031, W6Key, 1, getdate() from deleted