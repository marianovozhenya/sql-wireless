﻿CREATE TABLE [dbo].[W6RESERVED_COLUMN_NAMES] (
    [Name]   NVARCHAR (32) NOT NULL,
    [Origin] NVARCHAR (32) NOT NULL,
    CONSTRAINT [W6PK_RESERVED_COLUMN_NAMES] PRIMARY KEY CLUSTERED ([Name] ASC, [Origin] ASC)
);

