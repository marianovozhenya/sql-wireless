﻿CREATE TABLE [dbo].[W6COMPANIES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_250] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX250_2]
    ON [dbo].[W6COMPANIES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W6250_UPDATE_CACHED on W6COMPANIES for insert,update as if update(revision) insert into W6OPERATION_LOG select 250,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W6250_DELETE_CACHED on W6COMPANIES for delete as insert into W6OPERATION_LOG select 250, W6Key, 1, getdate() from deleted