﻿CREATE TABLE [dbo].[W6RP_DS_TRAVEL] (
    [AssignementKey]        INT             NULL,
    [EngineerKey]           INT             NULL,
    [DefaultTravelTime]     INT             NULL,
    [DefaultTravelDistance] DECIMAL (18, 2) NULL,
    [TravelType]            NCHAR (3)       NULL
);

