﻿CREATE TABLE [dbo].[W6SO_USER_GROUPS_CRUDS] (
    [W6Key]                INT NOT NULL,
    [CollectionID]         INT NOT NULL,
    [CanCreate]            INT NULL,
    [CanRead]              INT NULL,
    [CanUpdate]            INT NULL,
    [CanDelete]            INT NULL,
    [SoUserGroupFilterKey] INT NULL,
    CONSTRAINT [W6PK_50000193] PRIMARY KEY CLUSTERED ([W6Key] ASC, [CollectionID] ASC),
    CONSTRAINT [W6CK50000193_5_U] CHECK ([dbo].[W6FN_COUNT_W6SO_USER_GROUPS_CRUDS_PER_SoUserGroupFilterKey]([SoUserGroupFilterKey])=(1) OR [SoUserGroupFilterKey] IS NULL),
    CONSTRAINT [W6FK50000193_5_30000204_] FOREIGN KEY ([SoUserGroupFilterKey]) REFERENCES [dbo].[W6SO_USER_GROUP_FILTERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000193_5_30000204]
    ON [dbo].[W6SO_USER_GROUPS_CRUDS]([SoUserGroupFilterKey] ASC) WITH (FILLFACTOR = 50);

