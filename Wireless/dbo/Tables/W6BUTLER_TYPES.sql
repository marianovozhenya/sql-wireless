﻿CREATE TABLE [dbo].[W6BUTLER_TYPES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000030] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX10000030_2]
    ON [dbo].[W6BUTLER_TYPES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000030_UPDATE_CACHED on W6BUTLER_TYPES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000030,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000030_DELETE_CACHED on W6BUTLER_TYPES for delete as insert into W6OPERATION_LOG select 10000030, W6Key, 1, getdate() from deleted