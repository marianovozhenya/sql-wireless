﻿CREATE TABLE [dbo].[W6ABCAPACITYLIMITS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [District]           INT           NULL,
    [TaskType]           INT           NULL,
    [CapacityThreshold]  FLOAT (53)    NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_72] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK72_3_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK72_4_56_] FOREIGN KEY ([TaskType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX72_3_4]
    ON [dbo].[W6ABCAPACITYLIMITS]([District] ASC, [TaskType] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK72_4_56]
    ON [dbo].[W6ABCAPACITYLIMITS]([TaskType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK72_3_53]
    ON [dbo].[W6ABCAPACITYLIMITS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX72_2]
    ON [dbo].[W6ABCAPACITYLIMITS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W672_DELETE_CACHED on W6ABCAPACITYLIMITS for delete as insert into W6OPERATION_LOG select 72, W6Key, 1, getdate() from deleted
GO
create trigger W672_UPDATE_CACHED on W6ABCAPACITYLIMITS for insert,update as if update(revision) insert into W6OPERATION_LOG select 72,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted