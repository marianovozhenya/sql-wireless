﻿CREATE TABLE [dbo].[W6GROUPS] (
    [W6Key]             INT            NOT NULL,
    [Revision]          INT            NOT NULL,
    [CreatedBy]         NVARCHAR (128) NOT NULL,
    [TimeCreated]       DATETIME       NOT NULL,
    [CreatingProcess]   INT            NOT NULL,
    [ModifiedBy]        NVARCHAR (128) NOT NULL,
    [TimeModified]      DATETIME       NOT NULL,
    [ModifyingProcess]  INT            NOT NULL,
    [Name]              NVARCHAR (255) NULL,
    [Condition]         NVARCHAR (MAX) NULL,
    [Group_Type]        INT            NULL,
    [Script_Prog_ID]    NVARCHAR (256) NULL,
    [Is_Group_Unique]   INT            NULL,
    [Index_Type_String] NVARCHAR (32)  NULL,
    [Index_Body_String] NVARCHAR (512) NULL,
    [Paging]            NVARCHAR (MAX) NULL,
    [LastRefreshTime]   DATETIME       NULL,
    CONSTRAINT [W6PK_14] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX14_3]
    ON [dbo].[W6GROUPS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_14 ON W6GROUPS FOR DELETE AS  DELETE W6GROUP_OPTIONS FROM W6GROUP_OPTIONS, deleted WHERE  W6GROUP_OPTIONS.W6Key = deleted.W6Key
GO
create trigger W614_UPDATE_CACHED on W6GROUPS for insert,update as if update(revision) insert into W6OPERATION_LOG select 14,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W614_DELETE_CACHED on W6GROUPS for delete as insert into W6OPERATION_LOG select 14, W6Key, 1, getdate() from deleted