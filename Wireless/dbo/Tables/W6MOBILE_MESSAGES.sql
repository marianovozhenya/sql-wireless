﻿CREATE TABLE [dbo].[W6MOBILE_MESSAGES] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Engineer]         INT            NULL,
    [Task]             INT            NULL,
    [TaskDeleted]      INT            NULL,
    [MessageStatus]    INT            NULL,
    [MessageBody]      NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK110_3_8_] FOREIGN KEY ([Engineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK110_4_2_] FOREIGN KEY ([Task]) REFERENCES [dbo].[W6TASKS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK110_4_2]
    ON [dbo].[W6MOBILE_MESSAGES]([Task] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK110_3_8]
    ON [dbo].[W6MOBILE_MESSAGES]([Engineer] ASC) WITH (FILLFACTOR = 50);

