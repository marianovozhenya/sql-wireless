﻿CREATE TABLE [dbo].[W6LABORBILLABLE] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_80] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX80_2]
    ON [dbo].[W6LABORBILLABLE]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W680_UPDATE_CACHED on W6LABORBILLABLE for insert,update as if update(revision) insert into W6OPERATION_LOG select 80,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W680_DELETE_CACHED on W6LABORBILLABLE for delete as insert into W6OPERATION_LOG select 80, W6Key, 1, getdate() from deleted