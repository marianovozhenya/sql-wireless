﻿CREATE TABLE [dbo].[W6OFFLINE_OPERATION_SET] (
    [W6Key]                INT            NOT NULL,
    [Revision]             INT            NOT NULL,
    [CreatedBy]            NVARCHAR (128) NOT NULL,
    [TimeCreated]          DATETIME       NOT NULL,
    [CreatingProcess]      INT            NOT NULL,
    [ModifiedBy]           NVARCHAR (128) NOT NULL,
    [TimeModified]         DATETIME       NOT NULL,
    [ModifyingProcess]     INT            NOT NULL,
    [Name]                 NVARCHAR (64)  NULL,
    [Description]          NVARCHAR (512) NULL,
    [RelatedPlan]          INT            NULL,
    [OfflineOperationType] INT            NULL,
    [TimeToRun]            DATETIME       NULL,
    [Status]               INT            NULL,
    [ProcessStart]         DATETIME       NULL,
    [ProcessFinish]        DATETIME       NULL,
    [AgentID]              INT            NULL,
    [Retries]              INT            NULL,
    CONSTRAINT [W6PK_26] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK26_5_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK26_6_256_] FOREIGN KEY ([OfflineOperationType]) REFERENCES [dbo].[W6OFFLINE_OPERATION_SET_TYPE] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX26_7_0]
    ON [dbo].[W6OFFLINE_OPERATION_SET]([TimeToRun] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK26_6_256]
    ON [dbo].[W6OFFLINE_OPERATION_SET]([OfflineOperationType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK26_5_100]
    ON [dbo].[W6OFFLINE_OPERATION_SET]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_26 ON W6OFFLINE_OPERATION_SET FOR DELETE AS  DELETE W6OFFLINE_OPERATIONS FROM W6OFFLINE_OPERATIONS, deleted WHERE  W6OFFLINE_OPERATIONS.W6Key = deleted.W6Key