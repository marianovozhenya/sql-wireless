﻿CREATE TABLE [dbo].[W6SCHEDULE_LOCKS] (
    [LockDate]     DATETIME       NOT NULL,
    [CollectionID] INT            NOT NULL,
    [ObjectID]     INT            NOT NULL,
    [LockObjectID] INT            NOT NULL,
    [TimeCreated]  DATETIME       NULL,
    [ExtraData]    NVARCHAR (256) NULL,
    CONSTRAINT [W6PK_SCHEDULE_LOCKS] PRIMARY KEY CLUSTERED ([LockDate] ASC, [CollectionID] ASC, [ObjectID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX_SCHED_LOCKS_TIMECRTD]
    ON [dbo].[W6SCHEDULE_LOCKS]([TimeCreated] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_SCHED_LOCK_ID]
    ON [dbo].[W6SCHEDULE_LOCKS]([LockDate] ASC, [CollectionID] ASC, [LockObjectID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE TRIGGER W6SCHEDULE_LOCKS_INSERT ON W6SCHEDULE_LOCKS INSTEAD OF INSERT AS INSERT INTO W6SCHEDULE_LOCKS SELECT i.LockDate,i.CollectionID,i.ObjectID,i.LockObjectID,GetDate(),i.ExtraData from inserted i