﻿CREATE TABLE [dbo].[W6JOB_POSITIONS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [Name]             NVARCHAR (128) NULL,
    [ID]               NVARCHAR (128) NULL,
    [ValidityStart]    DATETIME       NULL,
    [ValidityFinish]   DATETIME       NULL,
    [Region]           INT            NULL,
    [District]         INT            NULL,
    [Team]             INT            NULL,
    [Manned]           INT            NULL,
    CONSTRAINT [W6PK_207] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK207_7_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK207_8_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK207_9_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX207_4]
    ON [dbo].[W6JOB_POSITIONS]([ID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK207_9_259]
    ON [dbo].[W6JOB_POSITIONS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK207_8_53]
    ON [dbo].[W6JOB_POSITIONS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK207_7_50]
    ON [dbo].[W6JOB_POSITIONS]([Region] ASC) WITH (FILLFACTOR = 50);

