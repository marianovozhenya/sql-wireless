﻿CREATE TABLE [dbo].[W6PART_LOCATIONS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (256)  NULL,
    [Description]      NVARCHAR (1024) NULL,
    [LocationID]       NVARCHAR (128)  NULL,
    [Address]          INT             NULL,
    [MobileKey]        NVARCHAR (128)  NULL,
    CONSTRAINT [W6PK_30000014] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000014_6_30000007_] FOREIGN KEY ([Address]) REFERENCES [dbo].[W6ADDRESSES] ([W6Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000014_3]
    ON [dbo].[W6PART_LOCATIONS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000014_7]
    ON [dbo].[W6PART_LOCATIONS]([MobileKey] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000014_6_30000007]
    ON [dbo].[W6PART_LOCATIONS]([Address] ASC) WITH (FILLFACTOR = 50);

