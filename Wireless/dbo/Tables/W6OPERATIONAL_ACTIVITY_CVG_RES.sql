﻿CREATE TABLE [dbo].[W6OPERATIONAL_ACTIVITY_CVG_RES] (
    [W6Key]                      INT            NOT NULL,
    [Revision]                   INT            NOT NULL,
    [CreatedBy]                  NVARCHAR (128) NOT NULL,
    [TimeCreated]                DATETIME       NOT NULL,
    [CreatingProcess]            INT            NOT NULL,
    [ModifiedBy]                 NVARCHAR (128) NOT NULL,
    [TimeModified]               DATETIME       NOT NULL,
    [ModifyingProcess]           INT            NOT NULL,
    [RelatedOperationalActivity] INT            NULL,
    [ActivityStatus]             NVARCHAR (64)  NULL,
    [CalculationTime]            DATETIME       NULL,
    [ExpirationTime]             DATETIME       NULL,
    [CoverageResult]             NVARCHAR (MAX) NULL,
    [ErrorCode]                  INT            NULL,
    [ErrorMessage]               NVARCHAR (MAX) NULL,
    [AgentName]                  NVARCHAR (64)  NULL,
    CONSTRAINT [W6PK_210] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6CK210_3_U] CHECK ([dbo].[W6FN_COUNT_W6OPERATIONAL_ACTIVITY_CVG_RES_PER_RelatedOperationalActivity]([RelatedOperationalActivity])=(1) OR [RelatedOperationalActivity] IS NULL),
    CONSTRAINT [W6FK210_3_208_] FOREIGN KEY ([RelatedOperationalActivity]) REFERENCES [dbo].[W6OPERATIONAL_ACTIVITIES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK210_3_208]
    ON [dbo].[W6OPERATIONAL_ACTIVITY_CVG_RES]([RelatedOperationalActivity] ASC) WITH (FILLFACTOR = 50);

