﻿CREATE TABLE [dbo].[W6SERVICE_NOTIFICATIONS] (
    [W6Key]                     INT             NOT NULL,
    [Revision]                  INT             NOT NULL,
    [CreatedBy]                 NVARCHAR (128)  NOT NULL,
    [TimeCreated]               DATETIME        NOT NULL,
    [CreatingProcess]           INT             NOT NULL,
    [ModifiedBy]                NVARCHAR (128)  NOT NULL,
    [TimeModified]              DATETIME        NOT NULL,
    [ModifyingProcess]          INT             NOT NULL,
    [UserNames]                 NVARCHAR (4000) NULL,
    [CustomDestination]         NVARCHAR (1024) NULL,
    [NotificationSource]        NVARCHAR (1024) NULL,
    [RelatedTask]               INT             NULL,
    [RelatedEngineer]           INT             NULL,
    [RelatedAssignment]         INT             NULL,
    [ServiceNotificationStatus] INT             NULL,
    [ServiceNotificationType]   INT             NULL,
    [CreationTime]              DATETIME        NULL,
    [ExpectedDueDate]           DATETIME        NULL,
    [Importance]                INT             NULL,
    [Description]               NVARCHAR (MAX)  NULL,
    [Latitude]                  INT             NULL,
    [Longitude]                 INT             NULL,
    [GISDataSource]             INT             NULL,
    [CountryID]                 INT             NULL,
    CONSTRAINT [W6PK_31000028] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK31000028_10_11000263_] FOREIGN KEY ([ServiceNotificationStatus]) REFERENCES [dbo].[W6SERVICE_NOTIFICATION_STATS] ([W6Key]),
    CONSTRAINT [W6FK31000028_11_11000264_] FOREIGN KEY ([ServiceNotificationType]) REFERENCES [dbo].[W6SERVICE_NOTIFICATION_TYPES] ([W6Key]),
    CONSTRAINT [W6FK31000028_19_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK31000028_8_8_] FOREIGN KEY ([RelatedEngineer]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK31000028_19_66]
    ON [dbo].[W6SERVICE_NOTIFICATIONS]([CountryID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK31000028_11_11000264]
    ON [dbo].[W6SERVICE_NOTIFICATIONS]([ServiceNotificationType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK31000028_10_11000263]
    ON [dbo].[W6SERVICE_NOTIFICATIONS]([ServiceNotificationStatus] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK31000028_8_8]
    ON [dbo].[W6SERVICE_NOTIFICATIONS]([RelatedEngineer] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX31000028_13_0]
    ON [dbo].[W6SERVICE_NOTIFICATIONS]([ExpectedDueDate] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_31000028 ON W6SERVICE_NOTIFICATIONS FOR DELETE AS  DELETE W6SERVICE_NOTIFICATIONS_DESTS FROM W6SERVICE_NOTIFICATIONS_DESTS, deleted WHERE  W6SERVICE_NOTIFICATIONS_DESTS.W6Key = deleted.W6Key