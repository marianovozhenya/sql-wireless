﻿CREATE TABLE [dbo].[W6RP_UNSCHEDULED_APPOINTMENTS] (
    [W6InstanceID]          INT            NOT NULL,
    [W6EntryID]             INT            NOT NULL,
    [TimeResolution_Start]  DATETIME       NULL,
    [TimeResolution_Finish] DATETIME       NULL,
    [Region_Key]            INT            NULL,
    [Region_Name]           NVARCHAR (256) NULL,
    [District_Key]          INT            NULL,
    [District_Name]         NVARCHAR (256) NULL,
    [TaskType_Key]          INT            NULL,
    [TaskType_Name]         NVARCHAR (256) NULL,
    [AssignedTasks_Count]   INT            NULL,
    [UnassignedTasks_Count] INT            NULL,
    [Task_CallID]           NVARCHAR (256) NULL,
    [Task_Customer]         NVARCHAR (256) NULL,
    [Task_Duration]         INT            NULL,
    [Task_Number]           INT            NULL,
    [Task_Priority]         INT            NULL,
    [Task_Status]           INT            NULL,
    [Task_Status_Name]      NVARCHAR (256) NULL,
    CONSTRAINT [PKRP_UNSCHEDULED_APPOINTMENTS] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);

