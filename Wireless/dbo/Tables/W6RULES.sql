﻿CREATE TABLE [dbo].[W6RULES] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (255)  NULL,
    [Importance]       INT             NULL,
    [Body]             NVARCHAR (MAX)  NULL,
    [ProgID]           NVARCHAR (256)  NULL,
    [EvaluationOrder]  INT             NULL,
    [PropagateStyle]   INT             NULL,
    [IsCached]         INT             NULL,
    [ViolationString]  NVARCHAR (2000) NULL,
    [RuleFamily]       INT             NULL,
    [ResourceCollID]   INT             NULL,
    [Flags]            INT             NULL,
    [Category]         NVARCHAR (64)   NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
create trigger W613_UPDATE_CACHED on W6RULES for insert,update as if update(revision) insert into W6OPERATION_LOG select 13,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W613_DELETE_CACHED on W6RULES for delete as insert into W6OPERATION_LOG select 13, W6Key, 1, getdate() from deleted