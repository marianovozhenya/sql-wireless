﻿CREATE TABLE [dbo].[W6SERVERS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [LastAgentsQuery]    DATETIME      NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX59_2]
    ON [dbo].[W6SERVERS]([Name] ASC) WITH (FILLFACTOR = 20);

