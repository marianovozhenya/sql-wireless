﻿CREATE TABLE [dbo].[W6ENG_DYNAMIC_DATA_LOCATION] (
    [W6Key]             INT           NOT NULL,
    [W6SubKey_1]        INT           NOT NULL,
    [LastUpdate]        DATETIME      NULL,
    [TimeModified]      DATETIME      NULL,
    [Latitude]          INT           NULL,
    [Longitude]         INT           NULL,
    [Altitude]          INT           NULL,
    [Heading]           INT           NULL,
    [Speed]             INT           NULL,
    [ID]                NVARCHAR (64) NULL,
    [TimeLocationTaken] DATETIME      NULL,
    [State]             INT           NULL,
    CONSTRAINT [W6PK_240002] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK240002_9_253_] FOREIGN KEY ([State]) REFERENCES [dbo].[W6ENGINEER_LOCATION_STATE] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX240002_2_3_7]
    ON [dbo].[W6ENG_DYNAMIC_DATA_LOCATION]([Latitude] ASC, [Longitude] ASC, [ID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX240002_1_K]
    ON [dbo].[W6ENG_DYNAMIC_DATA_LOCATION]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK240002_9_253]
    ON [dbo].[W6ENG_DYNAMIC_DATA_LOCATION]([State] ASC) WITH (FILLFACTOR = 50);

