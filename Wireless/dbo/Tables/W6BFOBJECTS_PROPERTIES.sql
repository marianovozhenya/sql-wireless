﻿CREATE TABLE [dbo].[W6BFOBJECTS_PROPERTIES] (
    [ObjectID]                 INT            NOT NULL,
    [PropertyID]               INT            NOT NULL,
    [PropertyName]             NVARCHAR (255) NULL,
    [IsMultiValue]             INT            NOT NULL,
    [ServerTypeCategory]       NVARCHAR (255) NULL,
    [ClientTypeCategory]       NVARCHAR (255) NULL,
    [ClientTypeSpecific]       NVARCHAR (255) NULL,
    [ServerPropertySet]        NVARCHAR (MAX) NULL,
    [ServerPropertyAttributes] INT            NULL,
    [ClientPropertyAttributes] INT            NULL,
    [CollectionIDForKey]       INT            NULL,
    [StringPropertyForKey]     INT            NULL,
    [PropertyTypeExtraInfo]    NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([ObjectID] ASC, [PropertyID] ASC),
    FOREIGN KEY ([ObjectID]) REFERENCES [dbo].[W6BFOBJECTS] ([ObjectID])
);

