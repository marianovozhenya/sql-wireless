﻿CREATE TABLE [dbo].[W6PLAN_PATHS] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [RelatedPlan]      INT            NULL,
    [Region]           INT            NULL,
    [District]         INT            NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK104_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK104_4_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK104_5_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK104_5_53]
    ON [dbo].[W6PLAN_PATHS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK104_4_50]
    ON [dbo].[W6PLAN_PATHS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK104_3_100]
    ON [dbo].[W6PLAN_PATHS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);

