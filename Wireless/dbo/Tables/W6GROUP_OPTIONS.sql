﻿CREATE TABLE [dbo].[W6GROUP_OPTIONS] (
    [W6Key]       INT             NOT NULL,
    [W6SubKey_1]  INT             NOT NULL,
    [Info_String] NVARCHAR (2000) NULL,
    CONSTRAINT [W6PK_140001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

