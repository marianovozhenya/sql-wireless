﻿CREATE TABLE [dbo].[W6PARTIALLY_CREATED_OBJECT] (
    [W6Key]            INT            NOT NULL,
    [Revision]         INT            NOT NULL,
    [CreatedBy]        NVARCHAR (128) NOT NULL,
    [TimeCreated]      DATETIME       NOT NULL,
    [CreatingProcess]  INT            NOT NULL,
    [ModifiedBy]       NVARCHAR (128) NOT NULL,
    [TimeModified]     DATETIME       NOT NULL,
    [ModifyingProcess] INT            NOT NULL,
    [CollectionID]     INT            NULL,
    [ObjectID]         INT            NULL,
    CONSTRAINT [W6PK_30000032] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX30000032_3_4]
    ON [dbo].[W6PARTIALLY_CREATED_OBJECT]([CollectionID] ASC, [ObjectID] ASC) WITH (FILLFACTOR = 20);

