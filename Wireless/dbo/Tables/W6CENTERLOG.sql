﻿CREATE TABLE [dbo].[W6CENTERLOG] (
    [W6Key]             INT            NOT NULL,
    [Revision]          INT            NOT NULL,
    [CreatedBy]         NVARCHAR (128) NOT NULL,
    [TimeCreated]       DATETIME       NOT NULL,
    [CreatingProcess]   INT            NOT NULL,
    [ModifiedBy]        NVARCHAR (128) NOT NULL,
    [TimeModified]      DATETIME       NOT NULL,
    [ModifyingProcess]  INT            NOT NULL,
    [HostName]          NVARCHAR (255) NULL,
    [MessageLevel]      NVARCHAR (64)  NULL,
    [MessageOperation]  NVARCHAR (255) NULL,
    [AdditionalContext] NVARCHAR (255) NULL,
    [Severity]          NVARCHAR (64)  NULL,
    [Feature]           NVARCHAR (255) NULL,
    [Product]           NVARCHAR (255) NULL,
    [ProcessID]         INT            NULL,
    [ThreadID]          INT            NULL,
    [MessageID]         INT            NULL,
    [UserName]          NVARCHAR (255) NULL,
    [SessionUserName]   NVARCHAR (255) NULL,
    [Duration]          FLOAT (53)     NULL,
    [Message]           NVARCHAR (MAX) NULL,
    [RequestID]         INT            NULL,
    [LocalAddress]      NVARCHAR (255) NULL,
    [RequestTimeStamp]  DATETIME       NULL,
    [ResponseTimeStamp] DATETIME       NULL,
    [MessageTime]       DATETIME       NULL,
    CONSTRAINT [W6PK_40000035] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX40000035_2__1_0]
    ON [dbo].[W6CENTERLOG]([TimeCreated] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX40000035_3_21]
    ON [dbo].[W6CENTERLOG]([HostName] ASC, [MessageTime] ASC) WITH (FILLFACTOR = 20);

