﻿CREATE TABLE [dbo].[W6PLAN_DEMANDS] (
    [W6Key]                   INT             NOT NULL,
    [Revision]                INT             NOT NULL,
    [CreatedBy]               NVARCHAR (128)  NOT NULL,
    [TimeCreated]             DATETIME        NOT NULL,
    [CreatingProcess]         INT             NOT NULL,
    [ModifiedBy]              NVARCHAR (128)  NOT NULL,
    [TimeModified]            DATETIME        NOT NULL,
    [ModifyingProcess]        INT             NOT NULL,
    [RelatedPlan]             INT             NULL,
    [PlanScenario]            INT             NULL,
    [DemandID]                NVARCHAR (64)   NULL,
    [DemandNumber]            INT             NULL,
    [Region]                  INT             NULL,
    [District]                INT             NULL,
    [TaskType]                INT             NULL,
    [Customer]                INT             NULL,
    [Probability]             FLOAT (53)      NULL,
    [DemandStatus]            INT             NULL,
    [EarliestStart]           DATETIME        NULL,
    [LatestFinish]            DATETIME        NULL,
    [StartTime]               DATETIME        NULL,
    [FinishTime]              DATETIME        NULL,
    [EstimatedWork]           INT             NULL,
    [PurchaseOrder]           NVARCHAR (64)   NULL,
    [Manager]                 NVARCHAR (64)   NULL,
    [Priority]                INT             NULL,
    [Billable]                INT             NULL,
    [BillableRate]            FLOAT (53)      NULL,
    [Description]             NVARCHAR (512)  NULL,
    [BinaryData]              NVARCHAR (1536) NULL,
    [ForecastedTravelTime]    INT             NULL,
    [TravelTime]              INT             NULL,
    [ForecastedWorkTime]      INT             NULL,
    [WorkTime]                INT             NULL,
    [ForecastedEstimatedWork] INT             NULL,
    CONSTRAINT [W6PK_4] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK4_10_60_] FOREIGN KEY ([Customer]) REFERENCES [dbo].[W6CUSTOMERS] ([W6Key]),
    CONSTRAINT [W6FK4_13_61_] FOREIGN KEY ([DemandStatus]) REFERENCES [dbo].[W6PLAN_DEMAND_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK4_3_100_] FOREIGN KEY ([RelatedPlan]) REFERENCES [dbo].[W6PLANS] ([W6Key]),
    CONSTRAINT [W6FK4_4_101_] FOREIGN KEY ([PlanScenario]) REFERENCES [dbo].[W6PLAN_SCENARIOS] ([W6Key]),
    CONSTRAINT [W6FK4_7_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK4_8_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK4_9_56_] FOREIGN KEY ([TaskType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX4_5_8]
    ON [dbo].[W6PLAN_DEMANDS]([DemandID] ASC, [District] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_13_61]
    ON [dbo].[W6PLAN_DEMANDS]([DemandStatus] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_10_60]
    ON [dbo].[W6PLAN_DEMANDS]([Customer] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_9_56]
    ON [dbo].[W6PLAN_DEMANDS]([TaskType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_8_53]
    ON [dbo].[W6PLAN_DEMANDS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_7_50]
    ON [dbo].[W6PLAN_DEMANDS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_4_101]
    ON [dbo].[W6PLAN_DEMANDS]([PlanScenario] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK4_3_100]
    ON [dbo].[W6PLAN_DEMANDS]([RelatedPlan] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX4_7_8_9_15]
    ON [dbo].[W6PLAN_DEMANDS]([Region] ASC, [District] ASC, [TaskType] ASC, [LatestFinish] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_4 ON W6PLAN_DEMANDS FOR DELETE AS  DELETE W6PLAN_DEMAND_REQUIRED_SKILLS FROM W6PLAN_DEMAND_REQUIRED_SKILLS, deleted WHERE  W6PLAN_DEMAND_REQUIRED_SKILLS.W6Key = deleted.W6Key