﻿CREATE TABLE [dbo].[W6TASKS_PARTS] (
    [W6Key]      INT           NOT NULL,
    [W6SubKey_1] INT           NOT NULL,
    [PartName]   INT           NULL,
    [Used]       INT           NULL,
    [Pulled]     INT           NULL,
    [InvSource]  NVARCHAR (7)  NULL,
    [Payer]      NVARCHAR (11) NULL,
    [SoldPrice]  FLOAT (53)    NULL,
    CONSTRAINT [W6PK_20013] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK20013_0_77_] FOREIGN KEY ([PartName]) REFERENCES [dbo].[W6PARTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK20013_0_77]
    ON [dbo].[W6TASKS_PARTS]([PartName] ASC) WITH (FILLFACTOR = 50);

