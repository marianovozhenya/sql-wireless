﻿CREATE TABLE [dbo].[W6PARTS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [PartNumber]         NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_77] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX77_2]
    ON [dbo].[W6PARTS]([Name] ASC) WITH (FILLFACTOR = 20);

