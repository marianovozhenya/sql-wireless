﻿CREATE TABLE [dbo].[W6AGENTS_OPERATIONS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Server]           NVARCHAR (64)   NULL,
    [AgentName]        NVARCHAR (64)   NULL,
    [StartTime]        DATETIME        NULL,
    [Status]           NVARCHAR (64)   NULL,
    [FinishTime]       DATETIME        NULL,
    [Integer1]         INT             NULL,
    [Integer2]         INT             NULL,
    [Integer3]         INT             NULL,
    [Integer4]         INT             NULL,
    [Integer5]         INT             NULL,
    [String1]          NVARCHAR (1024) NULL,
    [String2]          NVARCHAR (1024) NULL,
    [String3]          NVARCHAR (4000) NULL,
    [Boolean1]         INT             NULL,
    [Boolean2]         INT             NULL,
    [Boolean3]         INT             NULL,
    [Boolean4]         INT             NULL,
    [Boolean5]         INT             NULL,
    [LastError]        NVARCHAR (1024) NULL,
    [Integer6]         INT             NULL,
    [Integer7]         INT             NULL,
    [Integer8]         INT             NULL,
    [Integer9]         INT             NULL,
    [Integer10]        INT             NULL,
    [Float1]           FLOAT (53)      NULL,
    [Float2]           FLOAT (53)      NULL,
    [Date1]            DATETIME        NULL,
    [Date2]            DATETIME        NULL,
    [Date3]            DATETIME        NULL,
    [Date4]            DATETIME        NULL,
    [PollingID]        INT             NULL,
    [AgentID]          INT             NULL,
    CONSTRAINT [W6PK_21] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX21_2__4_0]
    ON [dbo].[W6AGENTS_OPERATIONS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX21_7_0]
    ON [dbo].[W6AGENTS_OPERATIONS]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_21 ON W6AGENTS_OPERATIONS FOR DELETE AS  DELETE W6AGENT_OPERATIONS_EXTRA_INFO FROM W6AGENT_OPERATIONS_EXTRA_INFO, deleted WHERE  W6AGENT_OPERATIONS_EXTRA_INFO.W6Key = deleted.W6Key  DELETE W6AGENTS_OPERATIONS_EXTRA_STR3 FROM W6AGENTS_OPERATIONS_EXTRA_STR3, deleted WHERE  W6AGENTS_OPERATIONS_EXTRA_STR3.W6Key = deleted.W6Key