﻿CREATE TABLE [dbo].[W6REPORTS_DTABLE] (
    [W6Key]       INT            NOT NULL,
    [W6SubKey_1]  INT            NOT NULL,
    [Name]        NVARCHAR (64)  NULL,
    [Description] NVARCHAR (256) NULL,
    [ProgID]      NVARCHAR (256) NULL,
    [NullValues]  INT            NULL,
    [Parameters]  NVARCHAR (MAX) NULL,
    CONSTRAINT [W6PK_70001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

