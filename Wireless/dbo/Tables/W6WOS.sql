﻿CREATE TABLE [dbo].[W6WOS] (
    [W6Key]               INT             NOT NULL,
    [Revision]            INT             NOT NULL,
    [CreatedBy]           NVARCHAR (128)  NOT NULL,
    [TimeCreated]         DATETIME        NOT NULL,
    [CreatingProcess]     INT             NOT NULL,
    [ModifiedBy]          NVARCHAR (128)  NOT NULL,
    [TimeModified]        DATETIME        NOT NULL,
    [ModifyingProcess]    INT             NOT NULL,
    [Name]                NVARCHAR (256)  NULL,
    [BinaryData]          NVARCHAR (4000) NULL,
    [WorkOrderID]         NVARCHAR (128)  NULL,
    [WorkOrderType]       INT             NULL,
    [StartDate]           DATETIME        NULL,
    [EndDate]             DATETIME        NULL,
    [Description]         NVARCHAR (1024) NULL,
    [Status]              INT             NULL,
    [PercentageCompleted] INT             NULL,
    CONSTRAINT [W6PK_30000012] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000012_11_10000011_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6PROJECT_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK30000012_7_10000010_] FOREIGN KEY ([WorkOrderType]) REFERENCES [dbo].[W6WO_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000012_2__4_0]
    ON [dbo].[W6WOS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000012_11_10000011]
    ON [dbo].[W6WOS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000012_7_10000010]
    ON [dbo].[W6WOS]([WorkOrderType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000012_6]
    ON [dbo].[W6WOS]([WorkOrderID] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_30000012 ON W6WOS FOR DELETE AS  DELETE W6WOS_WO_ITEMS FROM W6WOS_WO_ITEMS, deleted WHERE  W6WOS_WO_ITEMS.W6Key = deleted.W6Key