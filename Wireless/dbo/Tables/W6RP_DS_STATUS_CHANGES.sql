﻿CREATE TABLE [dbo].[W6RP_DS_STATUS_CHANGES] (
    [W6TaksKey]  INT           NOT NULL,
    [CallID]     NVARCHAR (64) NOT NULL,
    [TaskNumber] INT           NULL,
    [Status]     INT           NULL,
    [UpdateDate] DATETIME      NULL
);

