﻿CREATE TABLE [dbo].[W6RP_OBLIGATION_VIOLATION] (
    [W6InstanceID]                INT            NOT NULL,
    [W6EntryID]                   INT            NOT NULL,
    [TimeResolution_Start]        DATETIME       NULL,
    [TimeResolution_Finish]       DATETIME       NULL,
    [Region_Key]                  INT            NULL,
    [Region_Name]                 NVARCHAR (256) NULL,
    [District_Key]                INT            NULL,
    [District_Name]               NVARCHAR (256) NULL,
    [Resource_Contractor]         INT            NULL,
    [Resource_CrewForExternalUse] INT            NULL,
    [Resource_ID]                 NVARCHAR (256) NULL,
    [Resource_Internal]           INT            NULL,
    [Resource_Key]                INT            NULL,
    [Resource_Name]               NVARCHAR (256) NULL,
    [ResourceType_Name]           NVARCHAR (256) NULL,
    [MissedAppS_Num]              INT            NULL,
    [MissedAppF_Num]              INT            NULL,
    [MissedSLA_Num]               INT            NULL,
    [NotReqEng_Num]               INT            NULL,
    [NonWorkingTime_Num]          INT            NULL,
    CONSTRAINT [PKRP_OBLIGATION_VIOLATION] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);

