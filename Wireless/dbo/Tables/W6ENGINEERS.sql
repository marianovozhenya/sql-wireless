﻿CREATE TABLE [dbo].[W6ENGINEERS] (
    [W6Key]                     INT            NOT NULL,
    [Revision]                  INT            NOT NULL,
    [CreatedBy]                 NVARCHAR (128) NOT NULL,
    [TimeCreated]               DATETIME       NOT NULL,
    [CreatingProcess]           INT            NOT NULL,
    [ModifiedBy]                NVARCHAR (128) NOT NULL,
    [TimeModified]              DATETIME       NOT NULL,
    [ModifyingProcess]          INT            NOT NULL,
    [Name]                      NVARCHAR (255) NULL,
    [ID]                        NVARCHAR (64)  NULL,
    [Region]                    INT            NULL,
    [District]                  INT            NULL,
    [PostCode]                  NVARCHAR (64)  NULL,
    [Calendar]                  INT            NULL,
    [EngineerType]              INT            NULL,
    [Active]                    INT            NULL,
    [TravelSpeed]               INT            NULL,
    [Internal]                  INT            NULL,
    [Efficiency]                FLOAT (53)     NULL,
    [MobileClient]              INT            NULL,
    [AvailabilityFactor]        FLOAT (53)     NULL,
    [Latitude]                  INT            NULL,
    [Longitude]                 INT            NULL,
    [GISDataSource]             INT            NULL,
    [Street]                    NVARCHAR (64)  NULL,
    [City]                      NVARCHAR (64)  NULL,
    [State]                     NVARCHAR (64)  NULL,
    [CountryID]                 INT            NULL,
    [Contractor]                INT            NULL,
    [FixedTravel]               INT            NULL,
    [Company]                   INT            NULL,
    [Contract]                  INT            NULL,
    [IgnoreAllPreferences]      INT            NULL,
    [IgnoreFairnessCalculation] INT            NULL,
    [PreferenceApproved]        INT            NULL,
    [PreferenceApprovalDate]    DATETIME       NULL,
    [RosterApproved]            INT            NULL,
    [RosterApprovalDate]        DATETIME       NULL,
    [LunchBreakDuration]        INT            NULL,
    [LunchStartsFrom]           DATETIME       NULL,
    [HasDynamicData]            INT            NULL,
    [LocationID]                NVARCHAR (64)  NULL,
    [Crew]                      INT            NULL,
    [MobileClientSettings]      INT            NULL,
    [MobileWapClientSettings]   INT            NULL,
    [LastAllocationFinish]      DATETIME       NULL,
    [RelocationSource]          INT            NULL,
    [CrewForExternalUse]        INT            NULL,
    [MaxDistanceFromHB]         INT            NULL,
    [HomePhone]                 NVARCHAR (32)  NULL,
    [User_MobilePhone]          NVARCHAR (32)  NULL,
    [ADLoginID]                 NVARCHAR (64)  NULL,
    [LoginName]                 NVARCHAR (128) NULL,
    [Skill_Notes]               NVARCHAR (128) NULL,
    [StateSubdivision]          NVARCHAR (64)  NULL,
    [CitySubdivision]           NVARCHAR (64)  NULL,
    [SOLicenseInactive]         INT            NULL,
    [MobileWebClientSettings]   INT            NULL,
    [RequiredCrewSize]          INT            NULL,
    [PartsStock]                INT            NULL,
    [MobilePhone]               NVARCHAR (64)  NULL,
    [ExternalRefID]             NVARCHAR (128) NULL,
    [RegionalName]              NVARCHAR (64)  NULL,
    [RegionalCell]              NVARCHAR (64)  NULL,
    [RegionalEmail]             NVARCHAR (64)  NULL,
    [HeadTechName]              NVARCHAR (64)  NULL,
    [HeadTechCell]              NVARCHAR (64)  NULL,
    [HeadTechEmail]             NVARCHAR (64)  NULL,
    [ApproverLoginID]           NVARCHAR (64)  NULL,
    [SOUserGroup]               INT            NULL,
    [RelatedPartsLocation]      INT            NULL,
    [Org]                       INT            NULL,
    CONSTRAINT [W6PK_8] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK8_24_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key]),
    CONSTRAINT [W6FK8_27_250_] FOREIGN KEY ([Company]) REFERENCES [dbo].[W6COMPANIES] ([W6Key]),
    CONSTRAINT [W6FK8_42_17_] FOREIGN KEY ([MobileClientSettings]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK8_44_17_] FOREIGN KEY ([MobileWapClientSettings]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK8_46_8_] FOREIGN KEY ([RelocationSource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK8_5_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK8_58_17_] FOREIGN KEY ([MobileWebClientSettings]) REFERENCES [dbo].[W6USER_SETTINGS] ([W6Key]),
    CONSTRAINT [W6FK8_6_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK8_60_30000000_] FOREIGN KEY ([PartsStock]) REFERENCES [dbo].[W6PARTS_STOCKS] ([W6Key]),
    CONSTRAINT [W6FK8_70_30000203_] FOREIGN KEY ([SOUserGroup]) REFERENCES [dbo].[W6SO_USER_GROUPS] ([W6Key]),
    CONSTRAINT [W6FK8_71_30000014_] FOREIGN KEY ([RelatedPartsLocation]) REFERENCES [dbo].[W6PART_LOCATIONS] ([W6Key]),
    CONSTRAINT [W6FK8_72_88_] FOREIGN KEY ([Org]) REFERENCES [dbo].[W6ORGANIZATION] ([W6Key]),
    CONSTRAINT [W6FK8_8_3_] FOREIGN KEY ([Calendar]) REFERENCES [dbo].[W6CALENDARS] ([W6Key]),
    CONSTRAINT [W6FK8_9_55_] FOREIGN KEY ([EngineerType]) REFERENCES [dbo].[W6ENGINEER_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_72_88]
    ON [dbo].[W6ENGINEERS]([Org] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_71_30000014]
    ON [dbo].[W6ENGINEERS]([RelatedPartsLocation] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_70_30000203]
    ON [dbo].[W6ENGINEERS]([SOUserGroup] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_60_30000000]
    ON [dbo].[W6ENGINEERS]([PartsStock] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_58_17]
    ON [dbo].[W6ENGINEERS]([MobileWebClientSettings] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX8_0_41_57_3_4]
    ON [dbo].[W6ENGINEERS]([W6Key] ASC, [Crew] ASC, [SOLicenseInactive] ASC, [Name] ASC, [ID] ASC) WITH (FILLFACTOR = 20);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX8_3_6U]
    ON [dbo].[W6ENGINEERS]([ID] ASC, [District] ASC);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_46_8]
    ON [dbo].[W6ENGINEERS]([RelocationSource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_44_17]
    ON [dbo].[W6ENGINEERS]([MobileWapClientSettings] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_42_17]
    ON [dbo].[W6ENGINEERS]([MobileClientSettings] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_29_112]
    ON [dbo].[W6ENGINEERS]([Contract] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_27_250]
    ON [dbo].[W6ENGINEERS]([Company] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_24_66]
    ON [dbo].[W6ENGINEERS]([CountryID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_9_55]
    ON [dbo].[W6ENGINEERS]([EngineerType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_8_3]
    ON [dbo].[W6ENGINEERS]([Calendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_6_53]
    ON [dbo].[W6ENGINEERS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK8_5_50]
    ON [dbo].[W6ENGINEERS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX8_45]
    ON [dbo].[W6ENGINEERS]([LastAllocationFinish] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX8_0_3]
    ON [dbo].[W6ENGINEERS]([W6Key] ASC, [Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX8_5_6]
    ON [dbo].[W6ENGINEERS]([Region] ASC, [District] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX8_3]
    ON [dbo].[W6ENGINEERS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_8 ON W6ENGINEERS FOR DELETE AS  DELETE W6ENGINEERS_TOOLS FROM W6ENGINEERS_TOOLS, deleted WHERE  W6ENGINEERS_TOOLS.W6Key = deleted.W6Key  DELETE W6ENGINEERS_SKILLS FROM W6ENGINEERS_SKILLS, deleted WHERE  W6ENGINEERS_SKILLS.W6Key = deleted.W6Key  DELETE W6ENGINEERS_TRAINERS FROM W6ENGINEERS_TRAINERS, deleted WHERE  W6ENGINEERS_TRAINERS.W6Key = deleted.W6Key  DELETE W6ENGINEERS_PERIODIC_EFF FROM W6ENGINEERS_PERIODIC_EFF, deleted WHERE  W6ENGINEERS_PERIODIC_EFF.W6Key = deleted.W6Key  DELETE W6ENGINEERS_TIME_PHASED_SKILLS FROM W6ENGINEERS_TIME_PHASED_SKILLS, deleted WHERE  W6ENGINEERS_TIME_PHASED_SKILLS.W6Key = deleted.W6Key  DELETE W6ENGINEERS_WORKINGDISTRICTS FROM W6ENGINEERS_WORKINGDISTRICTS, deleted WHERE  W6ENGINEERS_WORKINGDISTRICTS.W6Key = deleted.W6Key
GO
create trigger W68_UPDATE_CACHED on W6ENGINEERS for insert,update as if update(revision) insert into W6OPERATION_LOG select 8,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W68_DELETE_CACHED on W6ENGINEERS for delete as insert into W6OPERATION_LOG select 8, W6Key, 1, getdate() from deleted