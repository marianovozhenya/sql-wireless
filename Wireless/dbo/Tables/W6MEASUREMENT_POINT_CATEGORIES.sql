﻿CREATE TABLE [dbo].[W6MEASUREMENT_POINT_CATEGORIES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_10000007] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX10000007_2]
    ON [dbo].[W6MEASUREMENT_POINT_CATEGORIES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W610000007_UPDATE_CACHED on W6MEASUREMENT_POINT_CATEGORIES for insert,update as if update(revision) insert into W6OPERATION_LOG select 10000007,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W610000007_DELETE_CACHED on W6MEASUREMENT_POINT_CATEGORIES for delete as insert into W6OPERATION_LOG select 10000007, W6Key, 1, getdate() from deleted