﻿CREATE TABLE [dbo].[W6CONTRACTS_TIME_PHASED_DATA] (
    [W6Key]          INT           NOT NULL,
    [W6SubKey_1]     INT           NOT NULL,
    [StartTime]      DATETIME      NULL,
    [FinishTime]     DATETIME      NULL,
    [PeriodicType]   INT           NULL,
    [Active]         INT           NULL,
    [PeriodicStart]  INT           NULL,
    [PeriodicFinish] INT           NULL,
    [ID]             NVARCHAR (64) NULL,
    [ContractInfo]   INT           NULL,
    [RestrictedDuty] INT           NULL,
    CONSTRAINT [W6PK_1120001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC),
    CONSTRAINT [W6FK1120001_7_113_] FOREIGN KEY ([ContractInfo]) REFERENCES [dbo].[W6CONTRACTS_INFO] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX1120001_1_K]
    ON [dbo].[W6CONTRACTS_TIME_PHASED_DATA]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK1120001_7_113]
    ON [dbo].[W6CONTRACTS_TIME_PHASED_DATA]([ContractInfo] ASC) WITH (FILLFACTOR = 50);

