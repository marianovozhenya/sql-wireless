﻿CREATE TABLE [dbo].[W6STEPS] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (256)  NULL,
    [Description]      NVARCHAR (1024) NULL,
    [StepID]           NVARCHAR (128)  NULL,
    [Duration]         INT             NULL,
    [BinaryData]       NVARCHAR (4000) NULL,
    CONSTRAINT [W6PK_30000010] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000010_2__4_0]
    ON [dbo].[W6STEPS]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000010_5]
    ON [dbo].[W6STEPS]([StepID] ASC) WITH (FILLFACTOR = 20);


GO
 CREATE TRIGGER W6TRIGGER_30000010 ON W6STEPS FOR DELETE AS  DELETE W6STEPS_REQUIRED_SKILLS FROM W6STEPS_REQUIRED_SKILLS, deleted WHERE  W6STEPS_REQUIRED_SKILLS.W6Key = deleted.W6Key  DELETE W6STEPS_REQUIRED_TOOLS FROM W6STEPS_REQUIRED_TOOLS, deleted WHERE  W6STEPS_REQUIRED_TOOLS.W6Key = deleted.W6Key