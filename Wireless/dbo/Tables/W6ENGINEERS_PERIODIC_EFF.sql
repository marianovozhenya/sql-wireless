﻿CREATE TABLE [dbo].[W6ENGINEERS_PERIODIC_EFF] (
    [W6Key]      INT        NOT NULL,
    [W6SubKey_1] INT        NOT NULL,
    [StartTime]  DATETIME   NULL,
    [FinishTime] DATETIME   NULL,
    [Efficiency] FLOAT (53) NULL,
    CONSTRAINT [W6PK_80005] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

