﻿CREATE TABLE [dbo].[W6ABSENCE_REQUESTS] (
    [W6Key]                   INT            NOT NULL,
    [Revision]                INT            NOT NULL,
    [CreatedBy]               NVARCHAR (128) NOT NULL,
    [TimeCreated]             DATETIME       NOT NULL,
    [CreatingProcess]         INT            NOT NULL,
    [ModifiedBy]              NVARCHAR (128) NOT NULL,
    [TimeModified]            DATETIME       NOT NULL,
    [ModifyingProcess]        INT            NOT NULL,
    [MobileKey]               NVARCHAR (64)  NULL,
    [Employee]                INT            NULL,
    [SubmitDate]              DATETIME       NULL,
    [StartTime]               DATETIME       NULL,
    [FinishTime]              DATETIME       NULL,
    [AbsenceType]             INT            NULL,
    [Status]                  INT            NULL,
    [AbsenceReason]           NVARCHAR (255) NULL,
    [CommentText]             NVARCHAR (MAX) NULL,
    [ApproverLoginID]         NVARCHAR (64)  NULL,
    [ApproverComment]         NVARCHAR (MAX) NULL,
    [ApprovalDate]            DATETIME       NULL,
    [NonAvailabilityType]     INT            NULL,
    [AbsenceTime]             INT            NULL,
    [CancelRequest]           INT            NULL,
    [RelatedVacationCalendar] INT            NULL,
    CONSTRAINT [W6PK_30000035] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK30000035_15_252_] FOREIGN KEY ([NonAvailabilityType]) REFERENCES [dbo].[W6NONAVAILABILITY_TYPES] ([W6Key]),
    CONSTRAINT [W6FK30000035_4_8_] FOREIGN KEY ([Employee]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK30000035_8_10000032_] FOREIGN KEY ([AbsenceType]) REFERENCES [dbo].[W6ABSENCE_TYPES] ([W6Key]),
    CONSTRAINT [W6FK30000035_9_10000031_] FOREIGN KEY ([Status]) REFERENCES [dbo].[W6ABSENCE_REQUEST_STATS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000035_18]
    ON [dbo].[W6ABSENCE_REQUESTS]([RelatedVacationCalendar] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000035_7_0]
    ON [dbo].[W6ABSENCE_REQUESTS]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000035_5_4]
    ON [dbo].[W6ABSENCE_REQUESTS]([SubmitDate] ASC, [Employee] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000035_9_10000031]
    ON [dbo].[W6ABSENCE_REQUESTS]([Status] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX30000035_6_4]
    ON [dbo].[W6ABSENCE_REQUESTS]([StartTime] ASC, [Employee] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000035_15_252]
    ON [dbo].[W6ABSENCE_REQUESTS]([NonAvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000035_4_8]
    ON [dbo].[W6ABSENCE_REQUESTS]([Employee] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK30000035_8_10000032]
    ON [dbo].[W6ABSENCE_REQUESTS]([AbsenceType] ASC) WITH (FILLFACTOR = 50);

