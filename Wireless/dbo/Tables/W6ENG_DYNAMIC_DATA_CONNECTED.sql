﻿CREATE TABLE [dbo].[W6ENG_DYNAMIC_DATA_CONNECTED] (
    [W6Key]          INT      NOT NULL,
    [W6SubKey_1]     INT      NOT NULL,
    [LastUpdate]     DATETIME NULL,
    [ConnectedSince] DATETIME NULL,
    CONSTRAINT [W6PK_240001] PRIMARY KEY CLUSTERED ([W6Key] ASC, [W6SubKey_1] ASC)
);

