﻿CREATE TABLE [dbo].[W6TECHNICAL_HISTORY] (
    [W6Key]            INT             NOT NULL,
    [Revision]         INT             NOT NULL,
    [CreatedBy]        NVARCHAR (128)  NOT NULL,
    [TimeCreated]      DATETIME        NOT NULL,
    [CreatingProcess]  INT             NOT NULL,
    [ModifiedBy]       NVARCHAR (128)  NOT NULL,
    [TimeModified]     DATETIME        NOT NULL,
    [ModifyingProcess] INT             NOT NULL,
    [Name]             NVARCHAR (256)  NULL,
    [ExternalRefID]    NVARCHAR (128)  NULL,
    [ReferenceDate]    DATETIME        NULL,
    [Description]      NVARCHAR (1024) NULL,
    CONSTRAINT [W6PK_30000006] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX30000006_2__4_0]
    ON [dbo].[W6TECHNICAL_HISTORY]([TimeModified] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX30000006_4]
    ON [dbo].[W6TECHNICAL_HISTORY]([ExternalRefID] ASC) WITH (FILLFACTOR = 20);

