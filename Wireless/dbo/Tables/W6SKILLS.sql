﻿CREATE TABLE [dbo].[W6SKILLS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6IX58_2]
    ON [dbo].[W6SKILLS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W658_UPDATE_CACHED on W6SKILLS for insert,update as if update(revision) insert into W6OPERATION_LOG select 58,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W658_DELETE_CACHED on W6SKILLS for delete as insert into W6OPERATION_LOG select 58, W6Key, 1, getdate() from deleted