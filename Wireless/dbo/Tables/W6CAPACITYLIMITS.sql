﻿CREATE TABLE [dbo].[W6CAPACITYLIMITS] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [District]           INT           NULL,
    [TaskType]           INT           NULL,
    [TimeResolution]     NVARCHAR (64) NULL,
    [CapacityThreshold]  FLOAT (53)    NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_70] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK70_3_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK70_4_56_] FOREIGN KEY ([TaskType]) REFERENCES [dbo].[W6TASK_TYPES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK70_4_56]
    ON [dbo].[W6CAPACITYLIMITS]([TaskType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK70_3_53]
    ON [dbo].[W6CAPACITYLIMITS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX70_2]
    ON [dbo].[W6CAPACITYLIMITS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W670_UPDATE_CACHED on W6CAPACITYLIMITS for insert,update as if update(revision) insert into W6OPERATION_LOG select 70,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W670_DELETE_CACHED on W6CAPACITYLIMITS for delete as insert into W6OPERATION_LOG select 70, W6Key, 1, getdate() from deleted