﻿CREATE TABLE [dbo].[W6COLLECTIONS] (
    [Collection_ID]        INT            NOT NULL,
    [Name]                 VARCHAR (255)  NOT NULL,
    [Storage_Type]         INT            NOT NULL,
    [Class_Category]       INT            NULL,
    [Keys_Num]             INT            NULL,
    [Father_Collection_ID] INT            NOT NULL,
    [First_Data]           INT            NULL,
    [Select_String]        NVARCHAR (MAX) NULL,
    [Insert_String]        NVARCHAR (MAX) NULL,
    [Insert_Types]         NVARCHAR (MAX) NULL,
    [Is_Auto_Index]        INT            NULL,
    [Delete_String]        VARCHAR (128)  NULL,
    [Delete_Types]         VARCHAR (128)  NULL,
    [Order_By_String]      VARCHAR (128)  NULL,
    [Range_Block_Number]   INT            NULL,
    [Range_Circle_Number]  INT            NULL,
    [Is_Product]           INT            NULL,
    [Extra_Data]           NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Collection_ID] ASC),
    UNIQUE NONCLUSTERED ([Collection_ID] ASC)
);


GO
 CREATE TRIGGER T_COLLECTIONS ON W6COLLECTIONS FOR DELETE AS DELETE W6INDEXES FROM W6INDEXES, deleted WHERE W6INDEXES.Collection_ID = deleted.Collection_ID  DELETE W6ATTRIBUTES FROM W6ATTRIBUTES, deleted WHERE W6ATTRIBUTES.Collection_ID = deleted.Collection_ID