﻿CREATE TABLE [dbo].[W6SO_VACATION_CALS_COUNTS] (
    [W6Key]             INT NOT NULL,
    [VacationCountName] INT NOT NULL,
    [TimeUnit1]         INT NULL,
    [TimeUnit2]         INT NULL,
    [TimeUnit3]         INT NULL,
    [TimeUnit4]         INT NULL,
    [TimeUnit5]         INT NULL,
    [TimeUnit6]         INT NULL,
    [TimeUnit7]         INT NULL,
    [TimeUnit8]         INT NULL,
    [TimeUnit9]         INT NULL,
    [TimeUnit10]        INT NULL,
    [TimeUnit11]        INT NULL,
    [TimeUnit12]        INT NULL,
    [TimeUnit13]        INT NULL,
    [TimeUnit14]        INT NULL,
    [TimeUnit15]        INT NULL,
    [TimeUnit16]        INT NULL,
    [TimeUnit17]        INT NULL,
    [TimeUnit18]        INT NULL,
    [TimeUnit19]        INT NULL,
    [TimeUnit20]        INT NULL,
    [TimeUnit21]        INT NULL,
    [TimeUnit22]        INT NULL,
    [TimeUnit23]        INT NULL,
    [TimeUnit24]        INT NULL,
    [TimeUnit25]        INT NULL,
    [TimeUnit26]        INT NULL,
    [TimeUnit27]        INT NULL,
    [TimeUnit28]        INT NULL,
    [TimeUnit29]        INT NULL,
    [TimeUnit30]        INT NULL,
    [TimeUnit31]        INT NULL,
    [TimeUnit32]        INT NULL,
    [TimeUnit33]        INT NULL,
    [TimeUnit34]        INT NULL,
    [TimeUnit35]        INT NULL,
    [TimeUnit36]        INT NULL,
    [TimeUnit37]        INT NULL,
    [TimeUnit38]        INT NULL,
    [TimeUnit39]        INT NULL,
    [TimeUnit40]        INT NULL,
    [TimeUnit41]        INT NULL,
    [TimeUnit42]        INT NULL,
    [TimeUnit43]        INT NULL,
    [TimeUnit44]        INT NULL,
    [TimeUnit45]        INT NULL,
    [TimeUnit46]        INT NULL,
    [TimeUnit47]        INT NULL,
    [TimeUnit48]        INT NULL,
    [TimeUnit49]        INT NULL,
    [TimeUnit50]        INT NULL,
    [TimeUnit51]        INT NULL,
    [TimeUnit52]        INT NULL,
    [TimeUnit53]        INT NULL,
    CONSTRAINT [W6PK_50000190] PRIMARY KEY CLUSTERED ([W6Key] ASC, [VacationCountName] ASC),
    CONSTRAINT [W6FK50000190_0_10000212_] FOREIGN KEY ([VacationCountName]) REFERENCES [dbo].[W6VACATION_CAL_COUNTS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK50000190_0_10000212]
    ON [dbo].[W6SO_VACATION_CALS_COUNTS]([VacationCountName] ASC) WITH (FILLFACTOR = 50);

