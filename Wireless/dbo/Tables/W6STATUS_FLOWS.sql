﻿CREATE TABLE [dbo].[W6STATUS_FLOWS] (
    [W6Key]                     INT             NOT NULL,
    [Revision]                  INT             NOT NULL,
    [Name]                      NVARCHAR (64)   NULL,
    [MainCollectionName]        NVARCHAR (128)  NULL,
    [StatusPropertyName]        NVARCHAR (128)  NULL,
    [AssociatedCollectionName]  NVARCHAR (128)  NULL,
    [AssociationPropertyName]   NVARCHAR (128)  NULL,
    [AssociationPropertyOnMain] INT             NULL,
    [Active]                    INT             NULL,
    [Description]               NVARCHAR (1024) NULL,
    [Stamp_TimeModified]        DATETIME        NULL,
    CONSTRAINT [W6PK_11000051] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX11000051_2]
    ON [dbo].[W6STATUS_FLOWS]([Name] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX11000051_3_0_1]
    ON [dbo].[W6STATUS_FLOWS]([MainCollectionName] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX11000051_5_0_1]
    ON [dbo].[W6STATUS_FLOWS]([AssociatedCollectionName] ASC, [W6Key] ASC, [Revision] ASC) WITH (FILLFACTOR = 20);

