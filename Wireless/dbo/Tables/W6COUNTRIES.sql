﻿CREATE TABLE [dbo].[W6COUNTRIES] (
    [W6Key]              INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [CountryID]          INT           NULL,
    [Stamp_TimeModified] DATETIME      NULL,
    CONSTRAINT [W6PK_66] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK66_3_66_] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[W6COUNTRIES] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK66_3_66]
    ON [dbo].[W6COUNTRIES]([CountryID] ASC) WITH (FILLFACTOR = 50);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX66_2]
    ON [dbo].[W6COUNTRIES]([Name] ASC) WITH (FILLFACTOR = 20);


GO
create trigger W666_UPDATE_CACHED on W6COUNTRIES for insert,update as if update(revision) insert into W6OPERATION_LOG select 66,W6Key, case when Revision = 1 then -1 else 0 end, getdate() from inserted
GO
create trigger W666_DELETE_CACHED on W6COUNTRIES for delete as insert into W6OPERATION_LOG select 66, W6Key, 1, getdate() from deleted