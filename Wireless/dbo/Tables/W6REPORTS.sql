﻿CREATE TABLE [dbo].[W6REPORTS] (
    [W6Key]                          INT            NOT NULL,
    [Revision]                       INT            NOT NULL,
    [CreatedBy]                      NVARCHAR (128) NOT NULL,
    [TimeCreated]                    DATETIME       NOT NULL,
    [CreatingProcess]                INT            NOT NULL,
    [ModifiedBy]                     NVARCHAR (128) NOT NULL,
    [TimeModified]                   DATETIME       NOT NULL,
    [ModifyingProcess]               INT            NOT NULL,
    [Name]                           NVARCHAR (64)  NULL,
    [Description]                    NVARCHAR (256) NULL,
    [ProductType]                    INT            NULL,
    [AllowChange]                    INT            NULL,
    [AllowDelete]                    INT            NULL,
    [EmptyPermutations]              INT            NULL,
    [TargetConnection]               INT            NULL,
    [TargetName]                     NVARCHAR (128) NULL,
    [ReportOutput]                   NVARCHAR (64)  NULL,
    [NumberOfRowsPerPage]            INT            NULL,
    [LastTableModeSuccessInstanceID] INT            NULL,
    [LastTableModeSuccessTime]       DATETIME       NULL,
    [DesignedToRunOn]                INT            NULL,
    [ProcessingUnit]                 INT            NULL,
    CONSTRAINT [W6PK_7] PRIMARY KEY CLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK7_9_11_] FOREIGN KEY ([TargetConnection]) REFERENCES [dbo].[W6CONNECTIONS] ([W6Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [W6IX7_10_9]
    ON [dbo].[W6REPORTS]([TargetName] ASC, [TargetConnection] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK7_9_11]
    ON [dbo].[W6REPORTS]([TargetConnection] ASC) WITH (FILLFACTOR = 50);


GO
 CREATE TRIGGER W6TRIGGER_7 ON W6REPORTS FOR DELETE AS  DELETE W6REPORTS_DTABLE FROM W6REPORTS_DTABLE, deleted WHERE  W6REPORTS_DTABLE.W6Key = deleted.W6Key  DELETE W6REPORTS_GROUPS FROM W6REPORTS_GROUPS, deleted WHERE  W6REPORTS_GROUPS.W6Key = deleted.W6Key  DELETE W6REPORTS_CALCULATORS FROM W6REPORTS_CALCULATORS, deleted WHERE  W6REPORTS_CALCULATORS.W6Key = deleted.W6Key  DELETE W6REPORTS_INSTANCES FROM W6REPORTS_INSTANCES, deleted WHERE  W6REPORTS_INSTANCES.W6Key = deleted.W6Key