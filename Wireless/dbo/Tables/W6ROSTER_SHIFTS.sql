﻿CREATE TABLE [dbo].[W6ROSTER_SHIFTS] (
    [W6Key]                 INT             NOT NULL,
    [Revision]              INT             NOT NULL,
    [CreatedBy]             NVARCHAR (128)  NOT NULL,
    [TimeCreated]           DATETIME        NOT NULL,
    [CreatingProcess]       INT             NOT NULL,
    [ModifiedBy]            NVARCHAR (128)  NOT NULL,
    [TimeModified]          DATETIME        NOT NULL,
    [ModifyingProcess]      INT             NOT NULL,
    [Name]                  NVARCHAR (64)   NULL,
    [AllocatedResource]     INT             NULL,
    [Region]                INT             NULL,
    [District]              INT             NULL,
    [Team]                  INT             NULL,
    [StartTime]             DATETIME        NULL,
    [FinishTime]            DATETIME        NULL,
    [BreakDuration]         INT             NULL,
    [Description]           NVARCHAR (1024) NULL,
    [AvailabilityType]      INT             NULL,
    [DisplayColor]          INT             NULL,
    [ReplacementPositionID] NVARCHAR (128)  NULL,
    [WorkAgreementID]       NVARCHAR (128)  NULL,
    [RestrictedDuty]        INT             NULL,
    [PaidBreak]             INT             NULL,
    [ShiftStatus]           INT             NULL,
    [CommentText]           NVARCHAR (1024) NULL,
    [Spare]                 INT             NULL,
    [BinaryData]            NVARCHAR (1024) NULL,
    [RosterDemand]          INT             NULL,
    [RosterDemandType]      INT             NULL,
    [ExternalRefID]         NVARCHAR (128)  NULL,
    [InSyncWithCalendar]    INT             NULL,
    [ActualStartTime]       DATETIME        NULL,
    [ActualFinishTime]      DATETIME        NULL,
    CONSTRAINT [W6PK_200] PRIMARY KEY NONCLUSTERED ([W6Key] ASC),
    CONSTRAINT [W6FK200_12_68_] FOREIGN KEY ([AvailabilityType]) REFERENCES [dbo].[W6AVAILABILITY_TYPES] ([W6Key]),
    CONSTRAINT [W6FK200_18_260_] FOREIGN KEY ([ShiftStatus]) REFERENCES [dbo].[W6SHIFT_STATUSES] ([W6Key]),
    CONSTRAINT [W6FK200_23_202_] FOREIGN KEY ([RosterDemand]) REFERENCES [dbo].[W6ROSTER_DEMANDS] ([W6Key]),
    CONSTRAINT [W6FK200_24_266_] FOREIGN KEY ([RosterDemandType]) REFERENCES [dbo].[W6ROSTER_DEMAND_TYPES] ([W6Key]),
    CONSTRAINT [W6FK200_4_8_] FOREIGN KEY ([AllocatedResource]) REFERENCES [dbo].[W6ENGINEERS] ([W6Key]),
    CONSTRAINT [W6FK200_5_50_] FOREIGN KEY ([Region]) REFERENCES [dbo].[W6REGIONS] ([W6Key]),
    CONSTRAINT [W6FK200_6_53_] FOREIGN KEY ([District]) REFERENCES [dbo].[W6DISTRICTS] ([W6Key]),
    CONSTRAINT [W6FK200_7_259_] FOREIGN KEY ([Team]) REFERENCES [dbo].[W6TEAMS] ([W6Key])
);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_24_266]
    ON [dbo].[W6ROSTER_SHIFTS]([RosterDemandType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_23_202]
    ON [dbo].[W6ROSTER_SHIFTS]([RosterDemand] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX200_9_0]
    ON [dbo].[W6ROSTER_SHIFTS]([FinishTime] ASC, [W6Key] ASC) WITH (FILLFACTOR = 20);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_18_260]
    ON [dbo].[W6ROSTER_SHIFTS]([ShiftStatus] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_12_68]
    ON [dbo].[W6ROSTER_SHIFTS]([AvailabilityType] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_7_259]
    ON [dbo].[W6ROSTER_SHIFTS]([Team] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_6_53]
    ON [dbo].[W6ROSTER_SHIFTS]([District] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_5_50]
    ON [dbo].[W6ROSTER_SHIFTS]([Region] ASC) WITH (FILLFACTOR = 50);


GO
CREATE NONCLUSTERED INDEX [W6IX_FK200_4_8]
    ON [dbo].[W6ROSTER_SHIFTS]([AllocatedResource] ASC) WITH (FILLFACTOR = 50);


GO
CREATE CLUSTERED INDEX [W6IX200_8_4]
    ON [dbo].[W6ROSTER_SHIFTS]([StartTime] ASC, [AllocatedResource] ASC);


GO
 CREATE TRIGGER W6TRIGGER_200 ON W6ROSTER_SHIFTS FOR DELETE AS  DELETE W6ROSTER_SHIFTS_POSITIONS FROM W6ROSTER_SHIFTS_POSITIONS, deleted WHERE  W6ROSTER_SHIFTS_POSITIONS.W6Key = deleted.W6Key