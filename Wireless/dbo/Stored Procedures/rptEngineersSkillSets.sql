﻿



CREATE Procedure [dbo].[rptEngineersSkillSets] 

AS

SELECT
r.name as Region,
d.name as District,
e.id as EmployeeId,
e.name AS Engineer,
e.MaxDistanceFromHB,
s.name AS Skill,
es.Efficiency

FROM
w6engineers e (nolock)
inner join w6engineers_skills es (nolock) on e.w6key = es.w6key
inner join w6skills s (nolock) on es.skillkey = s.w6key
inner join w6districts d (nolock) on e.district = d.w6key
inner join w6regions r (nolock) on e.region = r.w6key and r.w6key <> '124035072'

where 
e.relocationsource is null
and
 e.active = '-1'
 and
 e.Internal = -1
and 
e.w6key not in (
'923836724',--Test Moe
'521212072',--test Gary
'252774506'--TEST ServiceAnalysts

)


ORDER BY Engineer