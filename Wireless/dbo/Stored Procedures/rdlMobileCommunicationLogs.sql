﻿
-- Joe Strickland: This will give us the ability to query the connection logs to see the last time a tech connected to the click location service and successfully sent their GPS location
CREATE Procedure [dbo].[rdlMobileCommunicationLogs]
	
AS

SELECT     

e.W6Key, 
e.Name, 
e.LoginName, 
e.ADLoginID, 
dd.HasLocationData, 
e.MobileClient, 
dc.LastUpdate AS Connection_LastUpdate, 
dc.ConnectedSince, 
dl.LastUpdate AS Location_LastUpdate, 
dl.Latitude, 
dl.Longitude

FROM         

W6ENGINEERS AS e 

INNER JOIN W6ENG_DYNAMIC_DATA AS dd ON e.W6Key = dd.Engineer 
INNER JOIN W6ENG_DYNAMIC_DATA_CONNECTED AS dc ON dd.W6Key = dc.W6Key 
INNER JOIN W6ENG_DYNAMIC_DATA_LOCATION AS dl ON dd.W6Key = dl.W6Key

WHERE     (e.LoginName IS NOT NULL)
order by e.name