﻿CREATE PROCEDURE fill_range_of_days 
AS 
BEGIN 
 declare @current_day datetime; 
 declare @inpDateMinimum datetime; 
 declare @inpDateMaximum datetime;    
 select @inpDateMinimum = '01/01/2000'; 
 select @inpDateMaximum = '01/31/2020'; 
 select @current_day = @inpDateMinimum; 
 delete from W6Dates; 
 while @current_day <= @inpDateMaximum 
 begin 
 insert into W6Dates values(@current_day, @current_day+1); 
 set @current_day = @current_day + 1; 
 end; 
 end;