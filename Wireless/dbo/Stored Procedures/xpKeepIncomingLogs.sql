﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[xpKeepIncomingLogs]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select 
MESSAGEIN,
MESSAGEOUT,
MessageIn_Time

into #tmp2
from W6IM_INCOMING_LOG (nolock) 
where 
(
	MESSAGEIN not like '%<SXPExtendedTaskGetAppointments Revision="7.5.0%'
	and 
	MESSAGEIN not like '<SXPTaskOperations Revision="7.5.0" Source="CMS">%'
)

delete from W6IM_INCOMING_LOG 
from W6IM_INCOMING_LOG loggy (nolock)
inner join #tmp2 tmp on tmp.MessageIn_Time = loggy.MessageIn_Time
where
(
	loggy.MESSAGEIN not like '%<SXPExtendedTaskGetAppointments Revision="7.5.0%'
	and 
	loggy.MESSAGEIN not like '<SXPTaskOperations Revision="7.5.0" Source="CMS">%'
)
  
  


END