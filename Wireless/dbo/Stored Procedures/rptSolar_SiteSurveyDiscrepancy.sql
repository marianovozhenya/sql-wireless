﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[rptSolar_SiteSurveyDiscrepancy]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select
a.AccountNo,
t.Customer,
t.InstallingOffice,
t.CallID as TicketNumber,
st.DateCreated as DateCreatedInCMS,
t.TimeCreated as TicketScheduledTime,
t.AppointmentStart as TicketScheduledForInClick,
s.StartTime as TicketScheduledForInCMS,
ts.Name as TicketStatus
--,tt.Name as DispatchCode

from W6TASKS t with (nolock) 
inner join W6TASK_STATUSES ts with (nolock) on t.status = ts.w6key
inner join W6TASK_TYPES tt with (nolock) on t.TaskType = tt.W6Key
inner join mssql05.hssalesdaily.dbo.srvticket st with (nolock) on t.CallID = st.TicketNum
inner join mssql05.hssalesdaily.dbo.accounts a with (nolock) on st.accountid = a.accountid
inner join mssql05.hssalesdaily.dbo.srvSchedule s with (nolock) on s.TicketID = st.TicketID

where
tt.Name = 'Solar Site Survey'
and s.StartTime > cast ( DateAdd( day, 1, GETDATE()) as DATE)
and cast(s.StartTime as Date) <> cast( t.AppointmentStart as Date)

order by InstallingOffice, ts.Name, t.AppointmentStart



END