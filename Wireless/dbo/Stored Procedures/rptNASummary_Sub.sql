﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE rptNASummary_Sub


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



Select 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     Else 'n/a'
     End as REgion,
Sum(Case When tech.Active = -1 Then 1 Else 0 End) as CurrentlyActiveTechs      
from w6engineers tech with (nolock) 
Inner Join  w6regions r with (nolock) on r.w6Key = tech.Region
where r.Name like 'Central%' or r.Name like 'Eastern%' or r.Name like 'Western%'
Group By 
Case When r.Name like 'Central%' Then 'Central'
     When r.Name like 'Eastern%' Then 'Eastern'
     When r.Name like 'Western%' Then 'Western'
     Else 'n/a'
     End
Order By 1     


END