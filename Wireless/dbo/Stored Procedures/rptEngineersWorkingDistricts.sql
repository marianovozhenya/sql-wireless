﻿




CREATE Procedure [dbo].[rptEngineersWorkingDistricts] 

AS

SELECT distinct
r.name as Region,
d.name as District,
e.name as Engineer


from
w6engineers e (nolock)
inner join w6engineers_workingdistricts ewd (nolock) on e.w6key = ewd.w6key
inner join w6districts d (nolock) on ewd.district = d.w6key
inner join w6engineers_skills es (nolock) on e.w6key = es.w6key
inner join w6skills s (nolock) on es.skillkey = s.w6key
inner join w6regions r (nolock) on e.region = r.w6key


WHERE
E.RELOCATIONSOURCE IS NULL
and
e.Active = -1
and
e.Internal = -1

ORDER BY E.NAME